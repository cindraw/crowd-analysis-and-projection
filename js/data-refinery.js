/*
*
* DATA REFINERY
*
* This function increases interpolation and granularity of raw data
* This function is called from the Individual.js module.
* Interpolation and granularity serve to :
*	- increase the resolution of the data points (higher interpolation)
*	- extrapolate additional data for higher detailed projection (higher granularity)
*	- analyse data to extrapolate thresholds useful to normalize data for consistent projection
*	- save on CPU cycles during projection by precalculating the projected details
*
* Data input :
*
*				{
*				 id: String,
*				 points: [
*				     {
*				       time: Integer,
*				       x: Integer,
*				       y: Integer
*				     },
*				    ]
*				}
*
* Data ouput :
*				 {
*					{
*				   		id: String,					.....	id
*				   		time: Integer,				.....	time at this position
*				   		arrive: Integer,			.....	arrival time
*				   		depart: Integer,			.....	departure time
*				   		totalTime: Integer,			.....	total time up to this moment
*				   		totalDistance: Integer,		.....	total distance covered up to this moment
*				   		y: Integer,					.....	x position at this moment
*				   		x: Integer,					.....	y position at this moment
*				   		averageSpeed: Integer,		.....	average speed up to this moment
*				   		speed: Integer,				.....	current speed
*				   		previousx: Integer,			.....	x position of previous moment
*				   		previousy: Integer			.....	y position of previous moment
*					},
*					...,
*					{	info:
*							last: 	{ 	the last data point (for easy access) 	},
*							stops: 	{
*											id: String,
*											stopbegin: Integer,
*											stopend: Integer,
*											stoplength: Integer - Integer,
*											stopcoordinates: { x: Integer, y: Integer }
*									}
*					}
*
*
*/


// increases interpolation and granularity of raw datapoints
function refineData(data) {

	let
		points = data.points,
		id = data.id,
		objGranular = {},
		objInterpolated = {}

	// sort in order of time lower to higher
	points.sort((a, b) => a.time > b.time ? 1 : -1)

	// calculate number of stops from raw data
	let numberOfStops = ((_points) => {
		let result = []
		_points.forEach((j, index) => {
			let n = _points[index + 1] || undefined
			if (n) {
				if (j.x === n.x && j.y === n.y) {
					result = [...result, {
						id: id,
						stopbegin: j.time,
						stopend: n.time,
						stoplength: n.time - j.time,
						stopcoordinates: { x: j.x, y: j.y }
					}]
				}
			}
		})
		return result.length ? result : null
	})(points)

	// find time of arrival, departure and total time
	const
		times = points.map(i => i.time),
		arrive = Math.min(...times),
		depart = Math.max(...times),
		totalTime = depart - arrive


	//create objects with time as keys
	points.map(i => objInterpolated[i.time] = { x: i.x, y: i.y })

	// because the data is not indexed (array) but keyed (object), this generator returns the next entry in the document when called
	let entries = getEntries()
	function* getEntries() {
		for (let i in objInterpolated) {
			yield { k: i, v: objInterpolated[i] }
		}
	}
	// get rid of the first entry so that it returns the next value when called :  obj[x+1]
	entries.next()

	// Increase resolution of data by interpolation and add granulation
	for (let i in objInterpolated) {

		let
			next = entries.next(),
			averageSpeed

		// interpolate and granulate data between current position and next position
		if (!next.done) {

			let
				// key value
				min = i,
				max = next.value.k,
				// coordinate
				minc = objInterpolated[i],
				maxc = next.value.v,
				// count from min
				j = parseInt(min)

			// fill granular data between min and max
			for (j; j <= max; j++) {

				let
					// determine upperlimit and lowerlimit for interpolated data
					upperlimit = max - min,
					lowerlimit = j - min,
					//percentage increments of interpolated points
					percentage = lowerlimit / upperlimit,
					// calculate current coordinates by percentage distance to endpoint from start point to end point
					newx = minc.x + ((maxc.x - minc.x) * percentage),
					newy = minc.y + ((maxc.y - minc.y) * percentage),
					// calculate previous object if exists else make current (affects only the first element)
					prevObj = objInterpolated[j - 1] ? objInterpolated[j - 1] : objInterpolated[j],
					prevTotalDistance = objInterpolated[j - 1] ? parseFloat(prevObj.totalDistance) : 0,
					prevSpeed = parseFloat(prevObj.speed)

				// find distance between point a and point b = dist/sec = speed
				const speed = ((nx, ny, px, py) => {
					let
						[a, b] = [nx >= px ? nx - px : px - nx, ny >= py ? ny - py : py - ny],
						c = a * a + b * b,
						result = Math.sqrt(c)
					return result
				})(nx = newx, ny = newy, px = prevObj.x, py = prevObj.y)

				let
					totalDistance = prevTotalDistance + speed,
					averageSpeed = totalDistance / totalTime

				objGranular = {
					id,
					time: j,
					arrive,
					depart,
					totalTime,
					totalDistance,
					averageSpeed,
					x: newx,
					y: newy,
					speed: speed,
					previousx: prevObj.x,
					previousy: prevObj.y
				}

				objInterpolated[j] = objGranular

			}

		}
	}
	//save last data point and stops for easy access to project
	objInterpolated['info'] = {
		last: objGranular,
		stops: numberOfStops ? { amount: numberOfStops.length, location: numberOfStops } : { amount: 0 }
	}
	return objInterpolated
}