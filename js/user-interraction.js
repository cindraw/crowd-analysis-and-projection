/*
*
* Mouse interractions with the canvas element
* Drag and Drop
*
*/

function addMouseInterraction() {
	ELEMENT.canvas.addEventListener('mousedown', onCanvasMouseDown, true)
}

function onCanvasMouseDown() {
	animationPlayPause()
	window.addEventListener('mousemove', scrubAnimation, true)
	window.addEventListener('mouseup', removeMouseInterraction, true)
}


// MOUSE DRAG for scrubbing

// start drag on canvas to scrub time
function scrubAnimation(e) {

	let
		mvx = e.movementX

	animationPlayPause('pause')
	// out of bounds
	if (G_.TIME.current < G_.TIME.min && mvx < 0 || G_.TIME.current > G_.TIME.max && mvx > 0) { return }
	// scrub
	let newTime = G_.TIME.current += mvx * 2
	G_.TIME.current = newTime <= G_.TIME.min || newTime >= G_.TIME.max + SETTINGS.tail_length ? G_.TIME.current : newTime
	drawData()
}

// stop dragging general
function removeMouseInterraction() { window.removeEventListener('mousemove', scrubAnimation, true) }