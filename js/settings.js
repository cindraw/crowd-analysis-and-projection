
let
	SETTINGS =
	{
		tail_length: 25,
		zoom: 0.1,
		projection_proportion: 1,
		fps: 30,
		speed_increment: 1,
		delta: 1
	}
