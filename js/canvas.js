
const ctx = ELEMENT.canvas.getContext('2d', { antialias: true })
const canvas_rect = ELEMENT.canvas.getBoundingClientRect()

// conditionals for which elements are drawn
let
	is_DRAW = {
		persistent: true,
		slowAndStop: true,
		tail: true,
		timebar: true,
		timetext: true,
		averageSpeed: true,
		averageSpeedText: true
	}

// canvas draw

function drawData() {

	const
		canvas_width = ELEMENT.canvas.width * SETTINGS.projection_proportion,
		canvas_height = ELEMENT.canvas.height * SETTINGS.projection_proportion,
		individuals = G_.INDIVIDUALS,
		time_minimum = G_.TIME.min,
		time_current = G_.TIME.current,
		time_max = G_.TIME.max,
		time_total = G_.TIME.total,
		speed_average = G_.SPEED.average,
		speed_maximum = G_.SPEED.max,
		speed_minimum = G_.SPEED.min,
		ratio_width = 1 / G_.RATIO.width,
		ratio_height = 1 / G_.RATIO.height

	ctx.clearRect(0, 0, ELEMENT.canvas.width, ELEMENT.canvas.height)


	// average speed graph
	if (is_DRAW.averageSpeed) showAverageSpeed(canvas_width, canvas_height, time_minimum, time_current, time_max, time_total, speed_average)


	for (i in individuals) {

		// projectThisData instances of Individuals
		individuals[i].projectThisData()

		// draw in time except for the first frame because speed is 0 at time_current 0
		for (let x = time_minimum + 1; x < time_current; x++) {


			let g = individuals[i].richData.points[x]


			let prev_g = x > time_minimum ? individuals[i].richData.points[x - 1] : g


			if (g && prev_g) {

				// extract rich data

				let
					time = g.time,
					current_x = g.x,
					current_y = g.y,
					previous_x = prev_g.x,
					previous_y = prev_g.y,
					// speed and color need to be fixed or floored because safari doesn't handle excessive digits, also speeds up
					speed = g.velocity.xy < speed_maximum ? parseFloat(Math.abs(g.velocity.xy / speed_maximum).toFixed(4)) : 1,
					min_speed_threshold = parseFloat((speed_minimum * 1.5).toFixed(4)),
					max_speed_threshold = parseFloat((speed_maximum * 0.2).toFixed(4))


				// data scale to canvas size
				current_x = Math.trunc(current_x * canvas_width * ratio_width)
				current_y = canvas_height - Math.trunc(current_y * canvas_height * ratio_height)  // invert y
				previous_x = Math.trunc(previous_x * canvas_width * ratio_width)
				previous_y = canvas_height - Math.trunc(previous_y * canvas_height * ratio_height)  // invert y

				// size of stroke and arcs
				let size = 3
				size = (size - size * speed).toFixed(4)


				// DRAW
				// projects paths
				if (is_DRAW.persistent) drawPersistentPath(current_x, current_y, previous_x, previous_y, size, speed)
				// paint paths where individuals are walking slowly or stopped
				if (is_DRAW.slowAndStop) paintSlowSpeedsAndStops(current_x, current_y, min_speed_threshold, max_speed_threshold, speed, size)
				// draw tail
				if (is_DRAW.tail) handleTailFade(time, speed, min_speed_threshold, current_x, current_y, previous_x, previous_y)

			}


		}
	}

	if (is_DRAW.timebar) showTimeBar(canvas_width, canvas_height)
	if (is_DRAW.timetext) showTimeText(canvas_width, 10)
	if (is_DRAW.averageSpeedText) showAverageSpeedText(canvas_width, canvas_height, time_minimum, time_current, time_max, time_total, speed_average)

}