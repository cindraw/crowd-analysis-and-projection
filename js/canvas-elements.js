function sayHigh() {
	let
		w_center = ELEMENT.canvas.width / 2,
		h_third = ELEMENT.canvas.height / 3,
		margin_l = ELEMENT.canvas.width / 20

	ctx.font = '18px Courier'
	ctx.fillStyle = 'orange'
	ctx.textAlign = 'center'
	ctx.fillText('Hello !', w_center, h_third)
	ctx.fill()
	ctx.fillStyle = 'rgb(200, 200, 200)'
	ctx.font = '14px Courier'
	ctx.textAlign = 'left'
	ctx.fillText('Click this area to begin crowd simulation.', margin_l, h_third + 20)
	ctx.fillText('Click and drag to scrub simulation.', margin_l, h_third + 40)
	ctx.fillText('Use menu to explore options.', margin_l, h_third + 60)
	ctx.textAlign = 'left'
	ctx.fillText('- by Cedric Indra Widmer', margin_l, h_third + 80)
	ctx.fillText('cedricindra@gmail.com', margin_l, h_third + 100)
	ctx.fill()
}

function showTimeText(x = 5, y = 12) {
	// showtime text
	x = Math.trunc(x * G_.TIME.current / G_.TIME.max)
	ctx.font = '10px Courier';
	ctx.fillStyle = 'rgb(200, 200, 200)'
	ctx.textAlign = 'left';
	ctx.fillText(G_.TIME.current, x, y);
	ctx.fill()
}

function showTimeBar(width, height) {
	// draw time bar
	if (G_.TIME.current < G_.TIME.max) {
		let
			w = Math.trunc(width * G_.TIME.current / G_.TIME.max),
			h = 1,
			x = 0,
			y = 0

		ctx.fillStyle = 'blue'
		ctx.fillRect(x, y, w, h)
	}
}

function showAverageSpeed(canvas_width, canvas_height, time_minimum, time_current, time_max, time_total, speed_average) {
	// draw average speed graph
	if (time_current > time_minimum + 1) {
		let xpos, ypos, sp
		let t = time_minimum + 1

		let grey = 50
		ctx.fillStyle = `rgb(${grey}, ${grey}, ${grey})`
		ctx.beginPath()
		ctx.moveTo(0, canvas_height)

		for (t; t < time_current; t++) {

			sp = speed_average[t]
			ypos = canvas_height - (canvas_height * sp)
			xpos = canvas_width * t / time_max

			ctx.lineTo(xpos, ypos)
		}
		ctx.lineTo(xpos, canvas_height)
		ctx.closePath()
		ctx.fill()
	}

}

function showAverageSpeedText(canvas_width, canvas_height, time_minimum, time_current, time_max, time_total, speed_average) {

	let sp = parseFloat(speed_average[time_current]).toFixed(4),
		posx = Math.trunc(time_current / time_max * canvas_width),
		posy = canvas_height - (canvas_height * sp),
		grey = 100,
		color = `rgb(${grey}, ${grey}, ${grey})`

	ctx.font = '10px Courier'
	ctx.textAlign = 'center'
	ctx.fillStyle = color
	ctx.fillText('av ' + sp, posx, posy - 4)
	ctx.fill()

	ctx.beginPath()
	ctx.moveTo(posx, canvas_height)
	ctx.lineTo(posx, posy)
	ctx.strokeStyle = color
	ctx.lineWidth = 0.5
	ctx.stroke()

}


function drawPersistentPath(current_x, current_y, previous_x, previous_y, size, speed) {
	// persistent stroke projects paths

	let
		h = Math.floor(speed * 234),
		s = 100,
		l = 50 - Math.floor(speed * 25),
		a = 0.2

	let _color = `hsl(${h}, ${s}%, ${l}%, ${a})`

	ctx.beginPath()
	ctx.moveTo(current_x, current_y)
	ctx.lineTo(previous_x, previous_y)
	ctx.strokeStyle = _color
	ctx.lineWidth = 1
	// ctx.lineWidth = size
	// ctx.lineCap = 'butt'
	ctx.stroke()
}

function paintSlowSpeedsAndStops(current_x, current_y, min_speed_threshold, max_speed_threshold, speed, size) {
	// paint paths where individuals are walking slowly or stopped

	let
		x = current_x - size / 2,
		y = current_y - size / 2,
		alpha = 0.1

	let
		h = Math.floor(speed * 234),
		s = 100,
		l = 50,
		a = alpha - speed * alpha

	let _color = `hsl(${h}, ${s}%, ${l}%, ${a})`

	ctx.fillStyle = _color
	size = speed < min_speed_threshold ? size *= 1.5 : speed > max_speed_threshold ? size = 0 : size

	ctx.fillStyle = speed === 0 ? 'white' : speed
	size = speed === 0 ? size * 1.2 : size


	ctx.beginPath()
	ctx.arc(x, y, size, 0, 2 * Math.PI)
	ctx.fill()

}

function handleTailFade(time, speed, min_speed_threshold, current_x, current_y, previous_x, previous_y) {

	// draw tail
	let tail = G_.TIME.current - SETTINGS.tail_length
	// if dot is within tail range draw and fade tail
	if (tail < time) {

		let percentTail = (time - tail) / SETTINGS.tail_length

		let
			h = Math.floor(speed * 234),
			s = 100,
			l = 50,
			a = percentTail

		let _color = `hsl(${h}, ${s}%, ${l}%, ${a})`

		ctx.beginPath()
		ctx.moveTo(current_x, current_y)
		ctx.lineTo(previous_x, previous_y)
		ctx.strokeStyle = speed > 0 && speed < min_speed_threshold ? 'none' : _color
		ctx.lineWidth = 3
		ctx.lineCap = 'butt'
		ctx.stroke()
	}
}