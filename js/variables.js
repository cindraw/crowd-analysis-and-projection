
// DOM ELEMENTS
const

	ELEMENT =

	{
		dashboard: document.getElementById('dashboard'),
		analysis: document.getElementById('analysis'),
		blackout: document.getElementById('blackout'),
		main: document.getElementById('main'),
		canvas: document.getElementById('canvas'),
		speed_indicator: document.getElementById('speed-indicator')
	}

// BUTTONS
const

	BUTTON =

	{
		play_pause: document.getElementById('bt_play_pause'),
		loop: document.getElementById('bt_loop'),
		restart: document.getElementById('bt_restart'),
		show_id: document.getElementById('bt_show_id'),
		show_data: document.getElementById('bt_show_data'),
		show_none: document.getElementById('bt_show_none'),
		show_analysis: document.getElementById('bt_analysis'),
		show_stops: document.getElementById('bt_show_stops'),
		speed_up: document.getElementById('bt_speed_up'),
		speed_down: document.getElementById('bt_speed_down')
	}

// GLOBAL VARIABLES
let

	G_ =

	{
		INDIVIDUALS: [],

		RATIO: {
			width: undefined,
			height: undefined
		},

		TIME: {
			max: undefined,
			min: undefined,
			total: undefined,
			current: 0
		},

		POSITION: {
			max: {
				x: undefined,
				y: undefined
			},
			min: {
				x: undefined,
				y: undefined
			}
		},

		SPEED: {
			max: undefined,
			min: undefined,
			average: undefined
		}
	}