/*
*
* This class is a customized Intervalometer with easy funcitons.
* This is set up in animation_engine.js
* It accepts an id=>String, time=>Integer, func=>Function
*
* Start and Stop commands from anywhere in the app
*
*/


class myInterval {

	constructor({
		id: id,
		time: time,
		func: func,
	})

	{
		this.id = id
		this.func = func
		this.time = time
		this.timer = setInterval(() => null, 0)
	}

	start() {

		this.stop()
		this.timer = setInterval(() => {
			this.func()
		}, this.time)
	}

	stop() {
		clearInterval(this.timer)
	}
}
