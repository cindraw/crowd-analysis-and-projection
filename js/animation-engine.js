/*
*
* This module controls the animation.
* It requires SETTINGS.fps from settings
* It is triggered from animationPlayPause() 'animation_playpause.js'
*
 */

// ANIMATION ENGINE

const

	requestAnimationFrame = window.requestAnimationFrame
		|| window.mozRequestAnimationFrame
		|| window.webkitRequestAnimationFrame
		|| window.msRequestAnimationFrame
		|| function (f) { return setTimeout(f, 1000 / 60) }, // simulate calling code 60

	cancelAnimationFrame = window.cancelAnimationFrame
		|| window.mozCancelAnimationFrame
		|| function (requestID) { clearTimeout(requestID) } //fall back

let

	ANIMATION =

	{
		bool: undefined,

		play() {
			this.bool = true
			this.drawFrame()
		},

		stop() {
			this.bool = false
			clearInterval(this.timeout)
		},

		reqAnimFrameRate() {
			if (this.bool) setTimeout(() => { requestAnimationFrame(this.drawFrame.bind(this)) }, 1000 / SETTINGS.fps)
		},

		drawFrame() {
			drawData()
			G_.TIME.current += SETTINGS.delta
			// handle end of timeline
			if (G_.TIME.current >= G_.TIME.max + SETTINGS.tail_length) {
				// if button is 'loop', play once
				if (BUTTON.loop.innerText === 'loop') {
					console.log('END SIMULATION')
					animationPlayPause('pause')
				} else {
					console.log('RESTART')
					G_.TIME.current = G_.TIME.min
					this.reqAnimFrameRate()
				}
			} else {
				this.reqAnimFrameRate()
			}
		}

	}
