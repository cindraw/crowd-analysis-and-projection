/*
*
* Calcuations are made here to make the app responsive
* This is called on init and on window.onresize
*
*/

function placeElementsOnStage() {
	// setup main size from data
	let windowInnerHeight = window.innerHeight
	ELEMENT.main.height = Math.trunc(SETTINGS.zoom * windowInnerHeight * G_.RATIO.height)
	ELEMENT.main.width = Math.trunc(SETTINGS.zoom * windowInnerHeight * G_.RATIO.width)
	// place DOM elements
	ELEMENT.main.style.width = ELEMENT.main.width + 'px'
	ELEMENT.main.style.height = ELEMENT.main.height + 'px'
	//setup canvas size from main
	ELEMENT.canvas.width = ELEMENT.main.width
	ELEMENT.canvas.height = ELEMENT.main.height
}
