/*
*
* app entry point
*
* sets an array of Individuals from DATA (data.js) and counts them
* analyzes the data
* activates the menu buttons
* adds mouse interractivity
* calculates responsiveness and places DOM elements
*
*/

function init() {

	G_.INDIVIDUALS = DATA.map(i => new Individual(i, ELEMENT.main))
	// counts number of individuals on stage
	Individual.countAmount(G_.INDIVIDUALS)
	analyzeData()
	// init speed indicator in floorboard menu
	ELEMENT.speed_indicator.innerHTML = SETTINGS.delta
	activateButtons()
	addMouseInterraction()
	placeElementsOnStage()
	sayHigh()
}


window.onload = () => init()
window.onresize = () => {
	placeElementsOnStage()
	drawData()
}
