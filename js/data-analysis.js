/*
*
*	Data Analysis :
*	- Extracts the dimensions of the area covered by the data: G_.RATIO.width, RATION_H
*	- Extrapolates min time, max time, total time
*	- Normalizes speed data from granular data and returns new max speed and min speed to be used in the projection
*
*	DOES NOT ALTER THE RAW AND GRANULAR DATA
*
*/

function analyzeData() {

	// find the dimensions of the area and time frame by analyzing the data
	const
		individuals = G_.INDIVIDUALS,
		{
			maxtime,
			mintime,
			maxx,
			maxy,
			minx,
			miny
		} = Individual.getMinMaxFromInstances(individuals)

	// time
	G_.TIME.max = maxtime
	G_.TIME.min = mintime
	G_.TIME.total = maxtime - mintime
	// position
	G_.POSITION.max.x = maxx
	G_.POSITION.max.y = maxy
	G_.POSITION.min.x = minx
	G_.POSITION.min.y = miny
	// setup ratios to be used to calculate proportions
	G_.RATIO.width = maxx - minx
	G_.RATIO.height = maxy - miny
	// time start = min time from data
	G_.TIME.current = mintime
	// get max speed from granulardata and set speed thresholds
	let minmaxspeed = Individual.getMaxSpeedFromRichData(individuals)
	G_.SPEED.max = minmaxspeed.max
	G_.SPEED.min = minmaxspeed.min
	// get average speed of all individuals at time
	G_.SPEED.average = Individual.analyseAverageSpeedAtTimeFromRichData(individuals)
}
