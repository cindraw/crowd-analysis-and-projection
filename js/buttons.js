/*
*
* Menu buttons are declared and handled here
* animationPlayPause() in its own module animation_playpause.js
*
*/

function activateButtons() {
	// located in ELEMENT.dashboard
	// handled in 'animation_playpause.js'
	BUTTON.play_pause.onclick = e => animationPlayPause()
	BUTTON.loop.onclick = e => animationLoop()

	// declared here
	BUTTON.restart.onclick = e => buttonRestart()
	BUTTON.show_id.onclick = e => buttonShowID()
	BUTTON.show_data.onclick = e => buttonShowData()
	BUTTON.show_none.onclick = e => buttonHideALL()
	BUTTON.show_analysis.onclick = e => buttonAnalysisOpen()
	BUTTON.show_stops.onclick = e => buttonShowStopsFromRawData()
	BUTTON.speed_down.onclick = e => buttonSpeed(SETTINGS.speed_increment * -1)
	BUTTON.speed_up.onclick = e => buttonSpeed(SETTINGS.speed_increment)
	// activated when analyis modal is shown
	ELEMENT.blackout.onclick = e => buttonAnalysisClose()
}


function buttonShowStopsFromRawData() {
	G_.INDIVIDUALS.map(i => i.showStops())
	setTimeout(() => document.querySelectorAll('.stop-projection').forEach(j => j.remove()), 2000)
}

function buttonRestart() {
	G_.TIME.current = G_.TIME.min
	animationPlayPause('play')
}

function buttonShowID() {
	G_.INDIVIDUALS.map(i => {
		i.showWhatInfo = 'id'
		i.projectThisData()
	})
}

function buttonShowData() {
	G_.INDIVIDUALS.map(i => {
		i.showWhatInfo = 'info'
		i.projectThisData()
	})
}

function buttonHideALL() {
	G_.INDIVIDUALS.map(i => {
		i.showWhatInfo = 'hideElementIDandAnalysis'
		i.projectThisData()
	})
}

function buttonAnalysisOpen() {
	ELEMENT.analysis.style.display = 'block'
	ELEMENT.blackout.style.display = 'block'
	animationPlayPause('pause')
	setTimeout(() => buttonAnalysisClose(), 20000)
}

function buttonAnalysisClose() {
	ELEMENT.analysis.style.display = 'none'
	ELEMENT.blackout.style.display = 'none'
}

function buttonSpeed(num) {
	newSpeed = SETTINGS.delta + num
	SETTINGS.delta = newSpeed >= 1 && newSpeed <= 20 ? newSpeed : SETTINGS.delta
	ELEMENT.speed_indicator.innerHTML = SETTINGS.delta
}