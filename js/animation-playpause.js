/*
*
* This module controls the aniamtion play pause
* It does so by using the value of the button.innertext to set state
*
*/

function animationPlayPause(action = undefined) {
	// if action is undefined, determines action by what the text is inside the button
	let text = !action ? BUTTON.play_pause.innerText : action
	// play or pause
	if (text === 'play') {
		ANIMATION.play()
		BUTTON.play_pause.innerText = 'pause'
		return
	}
	else if (text === 'pause') {
		ANIMATION.stop()
		BUTTON.play_pause.innerText = 'play'
		return
	}
}

function animationLoop() {

	let text = BUTTON.loop.innerText

	if (text === 'loop') {
		BUTTON.loop.innerText = 'play once'
	}
	else if (text === 'play once') {
		BUTTON.loop.innerText = 'loop'
	}

}
