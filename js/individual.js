/*
*
* This class creates instances of individuals on set based on the data.
* This module increases data interpolation and granulation to be used for detailed visualization (data_refinery.js)
* This module places instances in the main div that supperposes the canvas div to allow for increased functionality and projection.
*
*/

class

	Individual {

	// constructor input data: Object and location: DOM element
	constructor(data, location) {
		this.data = data
		this.location = location
		this.id = this.data.id
		// this.richData = refineData(this.data)
		this.richData = dataRefinery(data)
		this.interval
		this.dotLayer
		this.currentElement
		this.template
		this.infoLast
		this.init()
		this.showWhatInfo = 'id'
	}

	init() {
		console.log('init:', this.id)
		this.makeContainer()
		this.getThisEndStateAnalysis()
	}


	// USER END STATE INTEL

	// appends an analysis of all user activity to the ELEMENT.analysis div in the app
	getThisEndStateAnalysis() {
		this.infoLast = document.createElement('div')
		this.infoLast.innerHTML = this.template.analyse(this.richData)
		ELEMENT.analysis.append(this.infoLast)
	}


	// DATA PROJECTION

	// project the data on the stage
	// called from the canvas drawing engine and from within this class when there is a state change
	projectThisData(time_current = G_.TIME.current) {
		let d = this.richData.points[time_current]
		this.currentElement = d
		// move the projection
		if (d) this.moveThisProjection()
		// if undefined means there is no data for it and must be hidden
		else {
			this.hideElementIDandAnalysis()
		}
	}

	// move this.element.div on canvas
	moveThisProjection() {
		const
			{ x, y } = this.currentElement,
			left = Math.floor((x / G_.POSITION.max.x * ELEMENT.main.width) * SETTINGS.projection_proportion),
			top = Math.floor((ELEMENT.main.height - (y / G_.POSITION.max.y * ELEMENT.main.height)) * SETTINGS.projection_proportion)

		this.element.div.style.left = left + 'px'
		this.element.div.style.top = top + 'px'

		// what info is showed (labels)
		switch (this.showWhatInfo) {
			case 'id':
				this.showElementID()
				break
			case 'info':
				this.showThisAnalysis()
				break
			case 'hideElementIDandAnalysis':
				this.hideElementIDandAnalysis()
		}

	}


	// LABELS

	// project info
	showElementID() {
		this.element.div.innerHTML = this.template.id(this.id)
	}
	showThisAnalysis() {
		this.element.div.innerHTML = this.template.info(this.currentElement, this.richData)
	}
	hideElementIDandAnalysis() {
		this.element.div.innerHTML = ''
	}

	// project stops from raw data
	showStops() {
		// extract stop info from data
		let stops = this.richData.info.stops
		if (!!stops.points.length) {
			stops.points.forEach(i => {

				let
					left = Math.floor((i.x / G_.POSITION.max.x * ELEMENT.main.width) * SETTINGS.projection_proportion),
					top = Math.floor((ELEMENT.main.height - (i.y / G_.POSITION.max.y * ELEMENT.main.height)) * SETTINGS.projection_proportion)

				// create div and place on stage
				this.stopPoint = document.createElement('div')
				this.stopPoint.classList.add('stop-projection')
				this.stopPoint.style.top = top + 'px'
				this.stopPoint.style.left = left + 'px'
				ELEMENT.main.append(this.stopPoint)
			}
			)
		}
	}


	// CREATE DOM ELEMENTS

	// container
	makeContainer() {
		this.dotLayer = document.createElement('div')
		// this.dotLayer.data = this.data
		this.dotLayer.richData = this.richData
		this.dotLayer.id = this.id
		this.dotLayer.classList.add('dot-layer')
		this.location.appendChild(this.dotLayer)

		// interactivity = show analysis at time
		this.dotLayer.onmouseenter = e => {
			this.showThisAnalysis()
			animationPlayPause('pause')
			setTimeout(() => {
				this.hideElementIDandAnalysis()
				this.projectThisData()
			}, 3000)
		}
	}


	// TEMPLATES

	// template additional dom elements
	get template() {
		return {

			id(id) {
				return `<div class="dot-info" style="pointer-events:all;background:none">${id}</div>`
			},

			info(dd, richData) {
				// console.log(dd)
				let {
					x,
					y,
					velocity,
					time
				} = dd
				let {id, info} = richData
				let {averageVelocity, totalDistance} = info
				return `
						<div class="dot-info">
							<table class="tb">
								<caption class="caption">${id}</caption>
								<tr>
									<td class="title">time</td>
									<td class="data">${time}</td>
									<td class="title">speed</td>
									<td class="data">${(velocity.xy).toFixed(2)}</td>
								</tr>
								<tr>
									<td class="title">pos.x</td>
									<td class="data">${x.toFixed(2)}</td>
									<td class="title">pos.y</td>
									<td class="data">${y.toFixed(2)}</td>
									</tr>
									<tr>
									<td class="title">speed av</td>
									<td class="data">${averageVelocity.toFixed(2)}</td>
									<td class="title">distance</td>
									<td class="data">${totalDistance.toFixed(2)}</td>
									</tr>
							</table>
						</div>`

			},

			analyse(dd) {
				let {
					id,
					info
				} = dd
				let { arrival, departure, totalTime, stops, averageVelocity, totalDistance } = info
				return `

						<div class="dot-analysis-display">
							<table class="tb">
							<caption class="caption">${id}</caption>
							<tr>
								<td class="title">arrive</td>
								<td class="data">${arrival}</td>
								<td class="title">leave</td>
								<td class="data">${departure}</td>
								<td class="title">total</td>
								<td class="data">${totalTime}</td>
								</tr>
							<tr>
								<td class="title">stops</td>
								<td class="data">${stops.points.length}</td>
							</tr>
							<tr>
								<td class="title">av speed</td>
								<td class="data">${averageVelocity.toFixed(4)}</td>
							</tr>
							<tr>
								<td class="title">distance </td>
								<td class="data">${totalDistance.toFixed(4)}</td>
							</tr>
							</table>
						</div>`
			},

		}
	}

	// returns element from stage
	get element() {
		return {
			div: document.getElementById(this.id),
		}
	}


	//STATIC FUNCTIONS

	// count indivuduals in document
	static countAmount(nodes) {
		console.log('number of instances: ', nodes.length)
	}

	// analyse and return data from all instances in document
	static getMinMaxFromInstances(nodes) {
		let [maxtime, mintime, maxx, maxy, minx, miny] = [[], [], [], [], [], []]
		nodes.forEach(i => {
			let values = Object.values(i.richData.points)
			let times = values.map(j => j.time)
			let x = values.map(j => j.x)
			let y = values.map(j => j.y)
			maxtime = [...maxtime, Math.max(...times)]
			mintime = [...mintime, Math.min(...times)]
			maxx = [...maxx, Math.max(...x)]
			maxy = [...maxy, Math.max(...y)]
			minx = [...minx, Math.min(...x)]
			miny = [...miny, Math.min(...y)]
		})
		maxtime = Math.max(...maxtime)
		mintime = Math.min(...mintime)
		maxx = Math.max(...maxx)
		maxy = Math.max(...maxy)
		minx = Math.min(...minx)
		miny = Math.min(...miny)

		return {
			mintime, maxtime, maxx, maxy, minx, miny
		}
	}

	static getMaxSpeedFromRichData(g = nodes) {

		// this normalizes the speed flukes such as when some data points skews the speed too far into one direction such as when speed > 1)
		// However this doesn't affect the data per say
		// The result min and max is only used to smooth out the projection

		let array = []

		g.map(i => {
			return Object.values(i.richData.points).map(j => {
				let num = j.velocity.xy
				if (num) {
					return num.toFixed(2)
				}
			})
		}).map(i => array = [...array, ...i])

		array = array.filter(i => i > 0 && i < 1)
		let min = Math.min(...array)
		let max = Math.max(...array)

		return { min, max }
	}



	static analyseAverageSpeedAtTimeFromRichData(g = nodes) {

		let array = [],
			averages = {}
		for (let x = G_.TIME.min; x <= G_.TIME.max; x++) {

			for (let i in g) {
				let individual = g[i],
					data = individual.richData.points[x]
				let speed = data ? data.velocity ? data.velocity.xy : undefined : null
				array = speed ? [...array, speed] : array
			}
			if (array.length) {
				let sum = array.reduce((a, b) => a + b),
					avg = sum / array.length
				averages[x] = avg ? avg : 0
			}
		}

		return averages
	}
}
