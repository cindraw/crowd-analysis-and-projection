
function dataRefinery(data) {

	let points = data.points

	points.sort((a, b) => a.time > b.time ? 1 : -1)

	points = findStopsInRawData(points)
	points = increaseDataInterpolationAndGranularity(points)
	points = getVelocityBetweenPoints(points)
	data.info = extrapolateInfoFromData(points)
	data.points = points
	return data

	function findStopsInRawData(points) {
		return points.map((i, index) => {
			const
				final = points[index + 1]
			if (final) {
				if (i.x - final.x === 0 && i.y - final.y === 0) {
					i.stop = { length: final.time - i.time }
					return i
				}
			}
			return i
		})
	}

	function increaseDataInterpolationAndGranularity(points) {

		interpolated = new Object()

		points.forEach((i, index) => {

			const
				initial = points[index],
				final = points[index + 1]

			if (final) {
				const
					time_start = initial.time,
					time_end = final.time,
					total_time = time_end - time_start

				interpolated[time_start] = initial
				interpolated[time_end] = final

				for (let t = 1; t < total_time; t++) {
					let time = t + time_start
					let
						deltaX = final.x - initial.x,
						deltaY = final.y - initial.y,
						deltaT = final.time - initial.time,
						x = initial.x + (((deltaX * t) + deltaX / (deltaT) * t * t) / 2) / total_time,
						y = initial.y + (((deltaY * t) + deltaY / (deltaT) * t * t) / 2) / total_time

					// interpolated[time] = { time, x: parseFloat(x.toFixed(2)), y: parseFloat(y.toFixed(2)) }
					interpolated[time] = { time, x, y }
				}
			}
		})

		return interpolated
	}


	function getVelocityBetweenPoints(points) {

		Object.keys(points).map((i, index) => {
			let initial = points[i],
				final = points[parseInt(i) + 1],
				x, y, xy
			if (final) {
				x = final.x - initial.x
				y = final.y - initial.y
				xy = Math.sqrt(x * x + y * y)
			}
			points[i].velocity = { x, y, xy }
		})

		return points

	}

	function extrapolateInfoFromData(points) {
		let array = []

		Object.values(points).map(i => array.push(i))

		const
			firstpoint = array[0],
			lastpoint = array[array.length - 1],
			arrival = firstpoint.time,
			departure = lastpoint.time,
			totalTime = departure - arrival,
			distances = array.map(i => i.velocity.xy ? i.velocity.xy : 0),
			maxVelocity = Math.max(...distances),
			minVelocity = Math.min(...distances),
			totalDistance = distances.reduce((a, b) => a + b),
			averageVelocity = totalDistance / distances.length

		let stops = new Object()
		stops.points = array.filter(i => i.stop)
		stops.totaltime = stops.points.length ? stops.points.map(i => i.stop.length).reduce((a, b) => a + b) : 0

		const info = {
			arrival,
			departure,
			totalTime,
			maxVelocity,
			minVelocity,
			totalDistance,
			averageVelocity,
			stops
		}

		return info

	}
}