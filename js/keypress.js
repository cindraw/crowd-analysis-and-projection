/*
*
* Key Strokes
*
* SPACE : start/stop animation
* Left Arrow : scrub back
* Right Arrow : scrub forward
*
*/

window.onkeydown = e => {
	const key = e.which
	switch (key) {
		case 32: // space
			animationPlayPause()
			break
		case 39: // arrow right
			animationPlayPause('pause')
			G_.TIME.current += 1
			drawData()
			break
		case 37: // arrow left
			animationPlayPause('pause')
			G_.TIME.current -= 1
			drawData()
			break
		default:
			return false
	}
}