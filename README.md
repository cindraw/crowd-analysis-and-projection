# Crowd Analysis and Projection

Analysis and projection of crowd movements from raw data.

### [click for demo](https://www.cedricindra.com/transfer/digeiz/index.html)

#### Simulation entry point : index.html


## Application Flow
#### 1. Import JSON data and create instances of the class 'Individual'
#### 2. Class 'Individual' refines the data by increasing interpolation and granularity returning intel such as : 
  - speed at given time  
  - average speed up to a given time   
  - number of stops
  - time of arrival
  - time of departure
  - length of visit
#### 3. Analyse data determines intel useful for the projection of the data on the stage :
  -  **maximum time and minimum time** of simulation.  
  -  **maximum and minimum positions x , y** used to estimate the map dimensions (ratio) of the projection. This allows for an accurate representation of the data to scale on a plane. Useful for page responsiveness.  
  -  **maximum speed and minimum speed** is normalized so the projection is smoothed.  
  -  **average speed** of all individuals at any given point in time.  
#### 4. Using the intel extracted from the data :
  -  Place elements on stage  
  -  Create projection  


## Menu Buttons
#### Individual info on stage
  -  **show id**: id  
  -  **show intel**: all intel at current point in time  
  -  **hide**: hide all  
#### Holistic info
  -  **show stops**: show the stops extracted from the raw Data  
  -  **show intel**: show the aggregate of the intel for each individual at time[time.length-1]  
#### Animation controls
  -  **play**: play/pause  
  -  **loop**: loop animation  
  -  **restart**: restart animation from time[0]  
  -  **speed**: animation speed  

## Simulation Key
#### Current time is displayed on the top of the canvas as a text and scroll bar.  

#### Individual trajectories are color coded in accordance to speed at given time.  
  -   **FAST ->** blue - green - yellow - red **<- SLOW**

#### Trajectories that are walked slowly are painted with thick and vivid strokes.  
  - **FAST ->** thin - thick **<- SLOW**  
  - **FAST ->** desaturated - saturated **<- SLOW**  
    
#### Full stops (speed = 0) are painted as large white dots  

## User Interaction
#### Mouse, trackpad and touch gestures.
  -  **click**: start/stop simulation  
  -  **click-drag, touch-drag**: scrubs the simulation (like a DAW or Movie editor)  
  -  **id roll-over**: shows individual intel  
#### Key Strokes
  -  **Space**: play/pause  
  -  **Left Arrow**: scrub left by 1 unit of time  
  -  **Right Arrow**: scrub right by 1 unit of time  